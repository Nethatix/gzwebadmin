﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using RestSharp;

namespace gzWeb.Admin
{
    public class CdnHelper
    {
        /// <summary>
        /// 
        /// Return posted pre-load endpoint result
        /// 
        /// </summary>
        /// <param name="authResult"></param>
        /// <returns></returns>
        private static bool GetLoadPostedRes(AuthenticationResult authResult, string filename)
        {
            var url = string.Format(CDNUrl,
                                        SubscriptionId,
                                        ResourceGroupName,
                                        ProfileName,
                                        EndpointName,
                                        ActionName,
                                        ApiVersion);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + authResult.AccessToken);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(new { ContentPaths = new[] { $"/{filename}" } });
            var result = client.Post(request);

            if (result.ResponseStatus == ResponseStatus.Completed)
            {
                var tries = 0;
                while (result.StatusCode != HttpStatusCode.OK && tries < 10)
                {
                    tries++;
                    Task.Delay(1000);
                    client.BaseUrl = new Uri($"https://gzimages.azureedge.net/{filename}");
                    result = client.Get(new RestRequest(Method.GET));
                }
            }

            return result.StatusCode == HttpStatusCode.OK;




        }

        public static async Task<bool> UpdateCdn(string filename)
        {
            AuthenticationResult authResult = await GetAccessToken();

            return GetLoadPostedRes(authResult, filename);
        }

        private static async Task<AuthenticationResult> GetAccessToken()
        {
            AuthenticationContext authContext = new AuthenticationContext(Authority);
            ClientCredential credential = new ClientCredential(clientID, clientSecret);
            AuthenticationResult authResult = await authContext.AcquireTokenAsync("https://management.core.windows.net/", credential);

            return authResult;
        }

        private const string SubscriptionId = "d92ca232-a672-424c-975d-1dcf45a58b0b"; // {0}
        private const string ResourceGroupName = "gzcdn_rg";// {1}
        private const string ProfileName = "gzcdn";// {2}
        private const string EndpointName = "gzimages";// {3}
        // purge or load
        private const string ActionName = "load";// {4}
        private const string ApiVersion = "2016-04-02";// {5}

        private const string clientID = "5c773ef1-8932-491d-b8f0-3e718de08b37";
        private const string clientSecret = "GVxBTL2GuhBK08q8vcgYuuFv9ayB3YdVEtTdhT/YgOw=";
        private const string Authority = "https://login.microsoftonline.com/15c12dcc-c81c-4873-90ea-2aba66afc1f6/greenzorro.com";
        private const string CDNUrl = "https://management.azure.com/subscriptions/{0}/resourceGroups/{1}/providers/Microsoft.Cdn/Profiles/{2}/endpoints/{3}/{4}?api-version={5}";
        //$"https://management.azure.com/subscriptions/{SubscriptionId}/resourceGroups/{ResourceGroupName}/providers/Microsoft.Cdn/profiles/{ProfileName}/endpoints/{EndpointName}/load?api-version=2016-04-02";
    }
}