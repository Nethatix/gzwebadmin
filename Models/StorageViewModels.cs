﻿using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage.Blob;

namespace gzWeb.Admin.Models
{
    public class BlobItemViewModel
    {
        public string Uri { get; set; }
        public string Name { get; set; }
        public string CdnUri { get; set; }
    }

    public class StorageIndexViewModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public List<BlobItemViewModel> Items { get; set; }
    }
}