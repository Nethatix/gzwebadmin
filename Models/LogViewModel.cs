﻿using System;
using System.Collections.Generic;
using gzDAL.Models;

namespace gzWeb.Admin.Models
{
    public class LogEntryViewModel
    {
        public LogEntryViewModel(LogEntry entry)
        {
            Id = entry.Id;
            Application = entry.Application;
            Logged = entry.Logged;
            Level = entry.Level;
            Message = entry.Message;
            UserName = entry.UserName;
            ServerName = entry.ServerName;
            Port = entry.Port;
            Url = entry.Url;
            Https = entry.Https;
            ServerAddress = entry.ServerAddress;
            RemoteAddress = entry.RemoteAddress;
            Logger = entry.Logger;
            CallSite = entry.CallSite;
            Exception = entry.Exception;

            Source = entry.Application + "::" + (entry.Url.Contains(".logger") ? "Client" : "Server");
            ShortMessage = entry.Message.Length > 105
                                   ? entry.Message.Substring(0, 50) + " ... " + entry.Message.Substring(entry.Message.Length - 50, 50)
                                   : entry.Message;
        }

        public int Id { get; set; }
        public string Application { get; set; }
        public DateTime Logged { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public string UserName { get; set; }
        public string ServerName { get; set; }
        public string Port { get; set; }
        public string Url { get; set; }
        public bool Https { get; set; }
        public string ServerAddress { get; set; }
        public string RemoteAddress { get; set; }
        public string Logger { get; set; }
        public string CallSite { get; set; }
        public string Exception { get; set; }

        public string Source { get; private set; }
        public string ShortMessage { get; private set; }
    }

    public class LogViewModel
    {        
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public string LogLevel { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string UserSearchText { get; set; }
        public List<LogEntryViewModel> LogEntries { get; set; }
    }

}