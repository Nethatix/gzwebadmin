﻿using System.Collections.Generic;
using gzDAL.Models;

namespace gzWeb.Admin.Models
{
    public class DynamicPageViewModel
    {        
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public string SearchTerm { get; set; }
        public bool ShowDeleted { get; set; }
        public List<DynamicPage> Entries { get; set; }
    }

    public class DynamicPageEditViewModel
    {
        public DynamicPage Page { get; set; }
        public List<DynamicPageData> Data { get; set; }
    }

    public class DynamicPageTemplatesViewModel
    {
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public string SearchTerm { get; set; }
        public List<DynamicPageTemplate> Entries { get; set; }
    }
}