﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using gzWeb.Admin.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace gzWeb.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class StorageController : Controller
    {
        // GET: Storage
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Obsolete("")]
        public async Task<ActionResult> Create(string imageUrl, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
            {
                byte[] fileBytes = new byte[file.ContentLength];
                file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                var containerName = ConfigurationManager.AppSettings["blobStorageContainer"];

                var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["blobStorageConnectionString"]);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference(containerName);
                if (!container.Exists())
                    container.Create(BlobContainerPublicAccessType.Blob);

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(file.FileName);
                blockBlob.Properties.ContentType = file.ContentType;

                blockBlob.UploadFromByteArray(fileBytes, 0, fileBytes.Length);

                var cdnBaseUrl = ConfigurationManager.AppSettings["CdnBaseUrl"];
                if (!string.IsNullOrEmpty(cdnBaseUrl))
                    await CdnHelper.UpdateCdn(file.FileName);
            }

            return RedirectToAction("Index", "Storage");
        }

        [HttpPost]
        public async Task<JsonResult> Upload()
        {
            var files = Request.Files;
            if (files.Count == 0)
                return Json("No files found.");

            foreach (string fileIdx in files)
            {
                var file = Request.Files[fileIdx];
                if (file.ContentLength > 0)
                {
                    byte[] fileBytes = new byte[file.ContentLength];
                    file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    var containerName = ConfigurationManager.AppSettings["blobStorageContainer"];

                    var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["blobStorageConnectionString"]);
                    var blobClient = storageAccount.CreateCloudBlobClient();
                    var container = blobClient.GetContainerReference(containerName);
                    if (!container.Exists())
                        container.Create(BlobContainerPublicAccessType.Blob);

                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(file.FileName);
                    blockBlob.Properties.ContentType = file.ContentType;

                    blockBlob.UploadFromByteArray(fileBytes, 0, fileBytes.Length);

                    var cdnBaseUrl = ConfigurationManager.AppSettings["CdnBaseUrl"];
                    if (!string.IsNullOrEmpty(cdnBaseUrl))
                        await CdnHelper.UpdateCdn(file.FileName);
                }
            }

            return Json("OK");
        }

        [HttpPost]
        public JsonResult FetchBlobs(int page = 1, int pageSize = 10)
        {
            var container = GetContainer();

            var count = GetContainerBlobCount(container);
            var blobs = container.ListBlobs()
                                 .Skip(pageSize*(page - 1))
                                 .Take(pageSize)
                                 .Select(x =>
                                         {
                                             var filename = x.Uri.AbsoluteUri.Substring(x.Uri.AbsoluteUri.LastIndexOf("/") + 1);
                                             return new BlobItemViewModel
                                             {
                                                     Uri = x.Uri.ToString(),
                                                     Name = filename,
                                                     CdnUri = $"https://gzimages.azureedge.net/{filename}"

                                             };
                                         })
                                 .ToList();

            return Json(new
                        {
                                Blobs = blobs,
                                BlobsCount = count,
                                Pages = (int) Math.Ceiling((double) count/pageSize)
                        });
        }

        [HttpPost]
        public JsonResult DeleteBlob(string filename)
        {
            var container = GetContainer();
            var blobCount = GetContainerBlobCount(container);
            var blob = container.GetBlockBlobReference(filename);
            blob.Delete();
            SetContainerBlobCount(container, --blobCount);
            return Json("Ok");
        }

        private CloudBlobContainer GetContainer()
        {
            var containerName = ConfigurationManager.AppSettings["blobStorageContainer"];

            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["blobStorageConnectionString"]);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(containerName);
            if (!container.Exists())
                container.Create(BlobContainerPublicAccessType.Blob);

            return container;
        }

        private void SetContainerBlobCount(CloudBlobContainer container, int count)
        {
            container.Metadata["ItemCount"] = count.ToString();
            container.Metadata["CountUpdateTime"] = DateTime.UtcNow.Ticks.ToString();
            container.SetMetadata();
        }

        private int GetContainerBlobCount(CloudBlobContainer container)
        {
            container.FetchAttributes();

            var recountNeeded = false;
            string count, countUpdateTime;
            container.Metadata.TryGetValue("itemCount", out count);
            container.Metadata.TryGetValue("countUpdateTime", out countUpdateTime);

            if (string.IsNullOrEmpty(count) || string.IsNullOrEmpty(countUpdateTime))
            {
                recountNeeded = true;
            }
            else
            {
                var dateTime = new DateTime(long.Parse(countUpdateTime));
                if (Math.Abs(dateTime.Subtract(container.Properties.LastModified.Value.UtcDateTime).TotalSeconds) > 5)
                    recountNeeded = true;
            }

            int blobCount;
            if (recountNeeded)
            {
                blobCount = container.ListBlobs(null, false, BlobListingDetails.Metadata).Count();

                container.Metadata["ItemCount"] = blobCount.ToString();
                container.Metadata["CountUpdateTime"] = DateTime.UtcNow.Ticks.ToString();
                container.SetMetadata();
            }
            else
            {
                blobCount = int.Parse(count);
            }

            return blobCount;
        }
    }
}