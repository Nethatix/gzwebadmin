﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using gzDAL.Models;

namespace gzWeb.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class GameCategoryController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public GameCategoryController()
        {
            // TODO: (xdinos) inject
            _dbContext = new ApplicationDbContext();
        }

        // GET: GameCategory
        public ActionResult Index()
        {
            return View(_dbContext.GameCategories.ToList());
        }


        public ActionResult Create()
        {
            return View(new GameCategory());
        }

        [HttpPost]
        public ActionResult Create(GameCategory model)
        {
            if (!ModelState.IsValid)
                return View(model);

            model.Updated = DateTime.UtcNow;
            _dbContext.GameCategories.Add(model);
            _dbContext.SaveChanges();

            return RedirectToAction("Index", "GameCategory");
        }

        public ActionResult Details(int id)
        {
            return View(_dbContext.GameCategories.Single(x => x.Id == id));
        }

        public ActionResult Edit(int id)
        {
            return View(_dbContext.GameCategories.Single(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult Edit(GameCategory model, HttpPostedFileBase file)
        {
            if (!ModelState.IsValid)
                return View(model);

            model.Updated = DateTime.UtcNow;
            _dbContext.GameCategories.AddOrUpdate(model, _dbContext);
            _dbContext.SaveChanges();

            return RedirectToAction("Index", "GameCategory");
        }
    }
}