﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using gzDAL.Conf;
using gzDAL.Models;
using gzWeb.Admin.Models;
using Microsoft.AspNet.Identity.Owin;

namespace gzWeb.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ManageController : Controller
    {
        private ApplicationUserManager _userManager;
        private readonly ApplicationDbContext _dbContext;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ManageController()
        {
            // TODO: (xdinos) inject
            _dbContext = new ApplicationDbContext();
        }

        // GET: Admin/Users
        public ActionResult Users(int page = 1, int pageSize = 20, string searchTerm = null)
        {
            var query = _dbContext.Users.AsQueryable();
            if (!String.IsNullOrEmpty(searchTerm))
            {
                query = query.Where(x => x.UserName.Contains(searchTerm) ||
                                         x.FirstName.Contains(searchTerm) ||
                                         x.LastName.Contains(searchTerm) ||
                                         x.Email.Contains(searchTerm));
            }

            var totalPages = (int)Math.Ceiling((float)query.Count() / pageSize);

            return View(new UsersViewModel
            {
                CurrentPage=page,
                TotalPages = totalPages,
                SearchTerm = searchTerm,
                UsersEntries = query.OrderBy(x=>x.Id).Skip((page-1)*pageSize).Take(pageSize).ToList()
            });
        }

        public ActionResult UserDetails(int id)
        {
            var user = _dbContext.Users.Single(x => x.Id == id);
            var roles = _dbContext.Roles.ToList();
            var rolesOfUser = roles.Where(x => x.Users.Any(r => r.UserId == user.Id)).ToList();

            roles.RemoveAll(x => rolesOfUser.Any(r => r.Id == x.Id));

            var gmUserId = GetGmUserId(user);
            var isUserValid = gmUserId.HasValue && user.GmCustomerId.HasValue &&
                              user.GmCustomerId.Value == gmUserId.Value;

            return View(new UserViewModel
                        {
                                User = user,
                                IsUserValid = isUserValid,
                                GmUserId = isUserValid ? "" : gmUserId.HasValue ? Convert.ToString(gmUserId.Value) : "Not found",
                                Roles = roles,
                                RolesOfUser = rolesOfUser,
                        });
        }

        private int? GetGmUserId(ApplicationUser user)
        {
            var gmUserId = (int?) null;
            using (var command = _dbContext.Database.Connection.CreateCommand())
            {
                _dbContext.Database.Connection.Open();
                try
                {
                    command.CommandText =
                            "SELECT TOP 1 [User ID] FROM [PlayerRevRpt] WHERE [Username]=@username AND [Email address]=@email";
                    var paramUsername = command.CreateParameter();
                    paramUsername.ParameterName = "@username";
                    paramUsername.DbType = DbType.String;
                    paramUsername.Value = user.UserName;
                    command.Parameters.Add(paramUsername);
                    var paramemail = command.CreateParameter();
                    paramemail.ParameterName = "@email";
                    paramemail.DbType = DbType.String;
                    paramemail.Value = user.Email;
                    command.Parameters.Add(paramemail);

                    var result = command.ExecuteScalar();
                    if (result != DBNull.Value && result != null)
                        gmUserId = Convert.ToInt32(result, System.Globalization.CultureInfo.InvariantCulture);
                }
                catch (Exception exception)
                {

                }
                finally
                {
                        _dbContext.Database.Connection.Close();
                }
            }

            return gmUserId;
        }

        public ActionResult UserDelete(int id)
        {
            return View(_dbContext.Users.Single(x => x.Id == id));
        }

        [HttpPost, ActionName("UserDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult UserDeleteConfirmed(int id)
        {
            _dbContext.Users.Remove(_dbContext.Users.Single(x => x.Id == id));
            _dbContext.SaveChanges();

            return RedirectToAction("Users", "Manage");
        }

        public ActionResult UserEdit(int id)
        {
            var user = _dbContext.Users.Single(x => x.Id == id);
            var gmUserId = GetGmUserId(user);
            var isUserValid = gmUserId.HasValue && user.GmCustomerId.HasValue &&
                              user.GmCustomerId.Value == gmUserId.Value;

            return View(new UserEditViewModel
                        {
                                User = user,
                                IsUserValid = isUserValid,
                                GmUserId = isUserValid
                                                   ? ""
                                                   : gmUserId.HasValue
                                                             ? Convert.ToString(gmUserId.Value)
                                                             : "Not found"
                        });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserEdit(ApplicationUser model)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("UserEdit", "Manage", new {id = model.Id});

            var user = _dbContext.Users.Single(x => x.Id == model.Id);

            user.GmCustomerId = model.GmCustomerId;
            user.LastName = model.LastName;
            user.FirstName = model.FirstName;
            user.DisabledGzCustomer = model.DisabledGzCustomer;
            user.ClosedGzAccount = model.ClosedGzAccount;
            user.ActiveCustomerIdInPlatform = model.ActiveCustomerIdInPlatform;

            _dbContext.Users.AddOrUpdate(user);
            _dbContext.SaveChanges();

            return RedirectToAction("Users", "Manage");
        }


        //[HttpPost]
        //public ActionResult UserAddToRole(int userId, int roleId)
        //{
        //    var role = _dbContext.Roles.SingleOrDefault(x => x.Id == roleId);

        //    if (role)
        //    UserManager.AddToRole()
        //}

        #region Roles

        // GET: Admin/Users
        public ActionResult Roles()
        {
            return View(_dbContext.Roles.ToList());
        }

        public ActionResult RoleCreate()
        {
            return View(new CustomRole());
        }

        [HttpPost]
        public ActionResult RoleCreate(CustomRole model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _dbContext.Roles.Add(model);
            _dbContext.SaveChanges();

            return RedirectToAction("Roles", "Manage");
        }

        public ActionResult RoleEdit(int id)
        {
            var role = _dbContext.Roles.Single(x => x.Id == id);
            var users = _dbContext.Users.ToList();
            var usersOfRole = users.Where(x => x.Roles.Any(r => r.RoleId == role.Id)).ToList();
            return View(new RoleViewModel
            {
                Role = role,
                Users = users,
                UsersOfRole = usersOfRole
            });
        }
        
        [HttpPost]
        public ActionResult RoleEdit(RoleViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _dbContext.Roles.AddOrUpdate(model.Role);
            _dbContext.SaveChanges();

            return RedirectToAction("Roles", "Manage");
        }

        [HttpPost]
        public ActionResult RoleAddOrRemoveUser(CustomRole role,string submit, List<string> users, List<string> usersOfRole)
        {
            switch (submit)
            {
                case "Add":
                    AddUsers(role.Id, users);
                    break;
                case "Remove":
                    RemoveUsers(role.Id, usersOfRole);
                    break;
            }

            role = _dbContext.Roles.Single(x => x.Id == role.Id);
            var rusers = _dbContext.Users.ToList();
            var rusersOfRole = rusers.Where(x => x.Roles.Any(r => r.RoleId == role.Id)).ToList();
            return View("RoleEdit", new RoleViewModel
                                    {
                                            Role = role,
                                            Users = rusers,
                                            UsersOfRole = rusersOfRole
                                    });
        }

        public ActionResult RoleDetails(int id)
        {
            return View(_dbContext.Roles.Single(x => x.Id == id));
        }

        private void AddUsers(int roleId, List<string> users)
        {
            var role = _dbContext.Roles.Single(x => x.Id == roleId);
            foreach (var userId in users.Select(x=>Convert.ToInt32(x)))
            {
                var user = _dbContext.Users.Single(x => x.Id == userId);
                role.Users.Add(new CustomUserRole {RoleId = roleId, UserId = userId});
            }

            _dbContext.SaveChanges();
        }

        private void RemoveUsers(int roleId, List<string> users)
        {
            var role = _dbContext.Roles.Single(x => x.Id == roleId);
            foreach (var userId in users.Select(x => Convert.ToInt32(x)))
            {
                var user = role.Users.Single(x => x.UserId == userId);
                role.Users.Remove(user);
            }

            _dbContext.SaveChanges();
        }

        #endregion
    }
}