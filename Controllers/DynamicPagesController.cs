﻿using System;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using gzDAL.Models;
using gzWeb.Admin.Models;

namespace gzWeb.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class DynamicPagesController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public DynamicPagesController()
        {
            // TODO: (xdinos) inject
            _dbContext = new ApplicationDbContext();
        }

        #region DynamicPages

        public ActionResult Index(int page = 1, int pageSize = 20, string searchTerm = null, bool showDeleted=false)
        {
            var query = _dbContext.DynamicPages.AsQueryable();

            if (!showDeleted)
                query = query.Where(x => !x.Deleted);

            if (!string.IsNullOrEmpty(searchTerm))
                query = query.Where(x => x.Code.Contains(searchTerm));

            var totalPages = (int)Math.Ceiling((float)query.Count() / pageSize);
            return View("Index", new DynamicPageViewModel
                                 {
                                         CurrentPage = page,
                                         TotalPages = totalPages,
                                         SearchTerm = searchTerm,
                                         ShowDeleted = showDeleted,
                                         Entries = query.OrderBy(x => x.Id)
                                                        .Skip((page - 1)*pageSize)
                                                        .Take(pageSize)
                                                        .ToList()
                                 });
        }

        public ActionResult Create(int id)
        {
            return View(new DynamicPage
                        {
                                DynamicPageTemplate = _dbContext.DynamicPageTemplates.Single(x => x.Id == id),
                                LiveFrom = DateTime.UtcNow,
                                LiveTo = DateTime.UtcNow.AddDays(30)
                        });
        }
        
        [HttpPost]
        public ActionResult Create(DynamicPage model, DynamicPageData[] pageData)
        {
            model.DynamicPageTemplate = _dbContext.DynamicPageTemplates
                                                  .Single(x => x.Id == model.DynamicPageTemplateId);

            if (!ModelState.IsValid)
                return View(model);

            var regEx = new Regex(@"\{(\w+):(\w+)\}", RegexOptions.Compiled);
            model.Html = regEx.Replace(model.DynamicPageTemplate.Html,
                                       match =>
                                       {
                                           var propertyName = match.Groups[1].Value;
                                           var propertyType = match.Groups[2].Value;
                                           return pageData.Single(x => x.DataName == propertyName &&
                                                                       x.DataType == propertyType).DataValue;
                                       });
            model.Updated = DateTime.UtcNow;
            _dbContext.DynamicPages.Add(model);
            
            foreach (var pd in pageData)
            {
                _dbContext.DynamicPagesData.Add(new DynamicPageData
                                                {
                                                        DataName = pd.DataName,
                                                        DataType = pd.DataType,
                                                        DataValue = pd.DataValue,
                                                        DynamicPage = model,
                                                });
            }

            _dbContext.SaveChanges();

            return RedirectToAction("Index", "DynamicPages");
        }

        public ActionResult Delete(int id)
        {
            return View(_dbContext.DynamicPages.Single(x => x.Id == id));
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = _dbContext.DynamicPages.Single(x => x.Id == id);
            model.Deleted = true;
            _dbContext.DynamicPages.AddOrUpdate(model, _dbContext);
            _dbContext.SaveChanges();

            return RedirectToAction("Index", "DynamicPages");
        }

        public ActionResult Edit(int id)
        {
            var page = _dbContext.DynamicPages
                                    .Include("DynamicPageTemplate")
                                    .Single(x => x.Id == id);
            var data = _dbContext.DynamicPagesData
                                     .Where(x => x.DynamicPage.Id == page.Id)
                                     .ToList();
            return View(new DynamicPageEditViewModel
                        {
                                Page = page,
                                Data = data
                        });
        }

        [HttpPost]
        public ActionResult Edit(DynamicPageEditViewModel model)
        {
            model.Page.DynamicPageTemplate = _dbContext.DynamicPageTemplates
                                                 .Single(x => x.Id == model.Page.DynamicPageTemplateId);

            if (!ModelState.IsValid)
            {
                model.Data = _dbContext.DynamicPagesData
                                       .Where(x => x.DynamicPage.Id == model.Page.Id)
                                       .ToList();
                return View(model);
            }

            var regEx = new Regex(@"\{(\w+):(\w+)\}", RegexOptions.Compiled);
            model.Page.Html = regEx.Replace(model.Page.DynamicPageTemplate.Html,
                                            match =>
                                            {
                                                var propertyName = match.Groups[1].Value;
                                                var propertyType = match.Groups[2].Value;
                                                return model.Data.Single(x => x.DataName == propertyName &&
                                                                              x.DataType == propertyType).DataValue;
                                            });

            foreach (var pd in model.Data)
            {
                _dbContext.DynamicPagesData.AddOrUpdate(new DynamicPageData
                                                        {
                                                                Id = pd.Id,
                                                                DataName = pd.DataName,
                                                                DataType = pd.DataType,
                                                                DataValue = pd.DataValue,
                                                                DynamicPage = model.Page,
                                                                DynamicPageId = model.Page.Id
                                                        }, _dbContext);
            }

            model.Page.Updated = DateTime.UtcNow;
            _dbContext.DynamicPages.AddOrUpdate(model.Page, _dbContext);

            _dbContext.SaveChanges();

            return RedirectToAction("Index", "DynamicPages");
        }

        #endregion

        #region DynamicPageTemplates

        public ActionResult Templates(int page = 1, int pageSize = 20, string searchTerm = null)
        {
            var query = _dbContext.DynamicPageTemplates.AsQueryable();

            var totalPages = (int)Math.Ceiling((float)query.Count() / pageSize);
            return View("Templates", new DynamicPageTemplatesViewModel
                                     {
                                             CurrentPage = page,
                                             TotalPages = totalPages,
                                             SearchTerm = searchTerm,
                                             Entries =
                                                     query.OrderBy(x => x.Id)
                                                          .Skip((page - 1)*pageSize)
                                                          .Take(pageSize)
                                                          .ToList()
                                     });
        }

        public ActionResult TemplateCreate()
        {
            return View(new DynamicPageTemplate());
        }

        [HttpPost]
        public ActionResult TemplateCreate(DynamicPageTemplate model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _dbContext.DynamicPageTemplates.Add(model);
            _dbContext.SaveChanges();

            return RedirectToAction("Templates", "DynamicPages");
        }

        public ActionResult TemplateDetails(int id)
        {
            return View(_dbContext.DynamicPageTemplates.Single(x => x.Id == id));
        }

        public ActionResult TemplateEdit(int id)
        {
            return View(_dbContext.DynamicPageTemplates.Single(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult TemplateEdit(DynamicPageTemplate model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _dbContext.DynamicPageTemplates.AddOrUpdate(model, _dbContext);
            _dbContext.SaveChanges();

            return RedirectToAction("Templates", "DynamicPages");
        }

        #endregion
    }

    
}