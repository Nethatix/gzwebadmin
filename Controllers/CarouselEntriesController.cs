﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using gzDAL.Models;
using gzWeb.Admin.Models;
using Microsoft.Azure.Management.Cdn;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Rest;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using RestSharp;
using AuthenticationContext = Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext;
using System.Net;
using Newtonsoft.Json;

namespace gzWeb.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CarouselEntriesController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public CarouselEntriesController()
        {
            // TODO: (xdinos) inject
            _dbContext = new ApplicationDbContext();
        }

        // GET: Admin/CarouselEntries
        public ActionResult Index(int page = 1, int pageSize = 20, string searchTerm = null, bool showDeleted = false)
        {
            var query = _dbContext.CarouselEntries.AsQueryable();
            if (!showDeleted)
                query = query.Where(x => !x.Deleted);

            if (!String.IsNullOrEmpty(searchTerm))
                query = query.Where(x => x.Code.Contains(searchTerm));

            var totalPages = (int) Math.Ceiling((float) query.Count()/pageSize);
            return View("Index", new CarouselEntriesViewModel()
                                 {
                                         CurrentPage = page,
                                         TotalPages = totalPages,
                                         SearchTerm = searchTerm,
                                         ShowDeleted = showDeleted,
                                         Entries = query.OrderBy(x => x.Id)
                                                        .Skip((page - 1)*pageSize)
                                                        .Take(pageSize)
                                                        .ToList()
                                 });
        }

        public ActionResult Create()
        {
            return View(new CarouselEntry {LiveFrom = DateTime.Now, LiveTo = DateTime.Now.AddDays(30)});
        }

        [HttpPost]
        public ActionResult Create(CarouselEntry model)
        {
            if (!ModelState.IsValid)
                return View(model);
            
            model.Updated = DateTime.UtcNow;
            _dbContext.CarouselEntries.Add(model);
            _dbContext.SaveChanges();

            return RedirectToAction("Index", "CarouselEntries");
        }

        public ActionResult Details(int id)
        {
            return View(_dbContext.CarouselEntries.Single(x => x.Id == id));
        }

        public ActionResult Delete(int id)
        {
            return View(_dbContext.CarouselEntries.Single(x => x.Id == id));
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = _dbContext.CarouselEntries.Single(x => x.Id == id);
            model.Deleted = true;
            _dbContext.CarouselEntries.AddOrUpdate(model, _dbContext);
            _dbContext.SaveChanges();

            return RedirectToAction("Index", "CarouselEntries");
        }

        public ActionResult Edit(int id)
        {
            return View(_dbContext.CarouselEntries.Single(x => x.Id == id));
        }
        
        [HttpPost]
        public ActionResult Edit(CarouselEntry model)
        {
            if (!ModelState.IsValid)
                return View(model);

            model.Updated = DateTime.UtcNow;
            _dbContext.CarouselEntries.AddOrUpdate(model, _dbContext);
            _dbContext.SaveChanges();

            return RedirectToAction("Index", "CarouselEntries");
        }
    }
}